---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data below is as of 2018-12-31

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 410   | 100%        |
| Based in the US                           | 234   | 57.07%      |
| Based in the UK                           | 24    | 5.85%       |
| Based in the Netherlands                  | 15    | 3.66%       |
| Based in Other Countries                  | 137   | 33.41%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 410   | 100%        |
| Men                                       | 323   | 78%         |
| Women                                     | 87    | 21%         |
| Other Gender Identities                   | 0     | 0%          |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 36    | 100%        |
| Men in Leadership                         | 30    | 83.33%      |
| Women in Leadership                       | 6     | 16.67%      |
| Other Gender Identities                   | 0     | 0%          |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 198   | 100%        |
| Men in Development                        | 170   | 89%         |
| Women in Development                      | 28    | 11.03%      |
| Other Gender Identities                   | 0     | 0%          |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        |224    | 100%        |
| Asian                                     | 18    | 8.04%       |
| Black or African American                 | 4     | 1.79%       |
| Hispanic or Latino                        | 14    | 6.25%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.45%       |
| Two or More Races                         | 9     | 4.02%       |
| White                                     | 119   | 53.13%      |
| Unreported                                | 59    | 26.34%      |

| Race/Ethnicity in Development   (US Only) | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 70    | 100%        |
| Asian                                     | 8     | 11.43%      |
| Black or African American                 | 2     | 2.86%       |
| Hispanic or Latino                        | 6     | 8.57%       |
  Two or More Races                         | 3     | 4.29%       |
| White                                     | 36    | 51.43%      |
| Unreported                                | 15    | 21.43%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 29    | 100%        |
| Asian                                     | 4     | 13.79%      |
| Native Hawaiian or Other Pacific Islander | 1     | 3.45%       |
| White                                     | 13    | 44.83%      |
| Unreported                                | 11    | 37.93%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 410   | 100%        |
| Asian                                     | 38    | 9.27%       |
| Black or African American                 | 9     | 2.20%       |
| Hispanic or Latino                        | 21    | 5.12%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.24%       |
| Two or More Races                         | 12    | 2.93%       |
| White                                     | 208   | 50.73%      |
| Unreported                                | 121   | 29.51%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 198   | 100%        |
| Asian                                     | 22    | 11.11%      |
| Black or African American                 | 6     | 3.03%       |
| Hispanic or Latino                        | 13    | 6.57%       |
| Two or More Races                         | 6     | 3.03%       |
| White                                     | 96    | 48.48%      |
| Unreported                                | 55    | 27.78%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 36    | 100%        |
| Asian                                     | 4     | 11.11%      |
| Hispanic or Latino                        | 1     | 2.78%       |
  Native Hawaiian or Other Pacific Islander | 1     | 2.78%       |
| White                                     | 15    | 41.67%      |
| Unreported                                | 15    | 41.67%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 410   | 100%        |
| 18-24                                     | 17    | 4.15%       |
| 25-29                                     | 86    | 20.98%      |
| 30-34                                     | 124   | 30.24%      |
| 35-39                                     | 65    | 15.85%      |
| 40-49                                     | 78    | 19.02%      |
| 50-59                                     | 34    | 8.29%       |
| 60+                                       | 4     | 0.98%       |
| Unreported                                | 2     | 0.49%       |
