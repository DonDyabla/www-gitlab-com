---
layout: markdown_page
title: "Single Codebase Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property     | Value |
|--------------|-------|
| Date Created | March 6, 2019 |
| Date Ended   | TBD |
| Slack        | [#f_single_codebase](https://gitlab.slack.com/messages/CGDC5RRDX) (only accessible from within the company) |
| Google Doc   | [Single Codebase Working Group Agenda](https://docs.google.com/document/d/1TkkPY5lhqpRzO3wlRZzxA3XBYYiAaWJZPuHI4vuzySY/edit) (only accessible from within the company) |

## Business Goal

Increase engineering efficiency by removing the [busy work](https://en.wikipedia.org/wiki/Busy_work) created by working in two similar codebases by merging the GitLab Community Edition and GitLab Enterprise Edition repositories.

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Facilitator           | Marin Jankovski       | Engineering Manager, Delivery  |
| Backend Lead          | Yorick Peterse        | Staff Backend Engineer         |
| Frontend Lead         | Filipa Lacerda        | Senior Frontend Engineer       |
| Member                | Phil Hughes           | Senior Frontend Engineer       |
| Documentation Lead    | Achilleas Pipinellis  | Technical Writer               |
| Executive Stakeholder | Christopher Lefelhocz | Senior Director of Development |