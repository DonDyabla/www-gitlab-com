/* eslint-disable guard-for-in*/

$(function() {
  var tcData = {
    main: {
      'hour': 08,
      'minute': 30,
      'duration': 30,
      'dayS': 'Monday',
      'dayE': 'Thursday',
      'originTz': 'America/Los_Angeles'
    },
    apac: {
      'hour': 20,
      'minute': parseInt('00', 10),
      'duration': 30,
      'dayS': 'Monday',
      'originTz': 'America/Los_Angeles'
    }
  };

  function endDayHelper(timezone, tz, pstTime) {
    return {
      'PST': moment(timezone).day(tcData.main.dayE).format('dddd'),
      'UTC': moment(timezone).utc().day(tcData.main.dayE).format('dddd'),
      'USER': moment.tz(moment(pstTime).day(tcData.main.dayE), moment.tz.guess()).format('dddd')
    }[tz];
  }

  function setTimeDate(tc) {
    var now = moment().tz(tc.originTz);
    var pstTime = now.set({
      'day': tc.dayS,
      'hour': tc.hour,
      'minute': tc.minute,
      'second': 0
    });

    var userTime = moment.tz(pstTime, moment.tz.guess());
    var utcTime = moment.utc(pstTime);
    var timezones = {
      PST: pstTime,
      UTC: utcTime,
      USER: userTime
    };

    for (tz in timezones) {
      var cTZ = timezones[tz];

      var startT = cTZ.format('hh:mm a');
      var endT = cTZ.add(tc.duration, 'm').format('hh:mm a');
      var startD = cTZ.format('dddd');
      var endD = endDayHelper(cTZ, tz, pstTime);

      $('#' + timezone + '-' + 'abbr').html(cTZ.zoneAbbr());
      $('#' + timezone + '-' + tz).html(startT + ' to ' + endT + ' - ' + startD + (timezone === 'main' ? ' to ' + endD : ''));
    }
  }

  for (timezone in tcData) {
    setTimeDate(tcData[timezone]);
  }
});
