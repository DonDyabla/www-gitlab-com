---
layout: job_family_page
title: "Site Reliability Engineer"
---

Site Reliability Engineers are responsible for the keeping all user-facing
services (most notably GitLab.com) and many other GitLab production systems
running smoothly 24/7/365. SREs are a blend of operations gearheads and
software crafters that apply sound engineering principles, operational discipline
and mature automation, specializing in systems, whether it be networking, the Linux
kernel, or even a specific interest in scaling, algorithms, or distributed systems.

GitLab.com is a unique site and it brings unique challenges: it’s the biggest
GitLab instance in existence; in fact, it’s one of the largest single-tenancy
open-source SaaS sites on the internet. The experience of our team feeds back
into other engineering groups within the company, as well as to GitLab customers
running self-managed installations.

## As a SRE you will:

* Be on a PagerDuty rotation to respond to GitLab.com availability incidents and
  provide support for service engineers with customer incidents.
* Use your on-call shift to prevent incidents from ever happening.
* Manage our infrastructure with Chef, Terraform and Kubernetes.
* Make monitoring and alerting alert on symptoms and not on outages.
* [Document every action](https://gitlab.com/gitlab-com/runbooks) so your learnings turn into repeatable actions and then into automation.
* Use the GitLab product to run GitLab.com as a first resort and improve the product as much as possible
* Improve the deployment process to make it as boring as possible.
* Design, build and maintain core infrastructure pieces that allow GitLab scaling to support hundred of thousands of concurrent users.
* Debug production issues across services and levels of the stack.
* Plan the growth of GitLab's infrastructure.

## You may be a fit to this role if you:

* Think about systems - edge cases, failure modes, behaviors, specific implementations.
* Know your way around Linux and the Unix Shell.
* Know what is the use of config management systems like Chef (the one we use)
* Have strong programming skills - Ruby and/or Go
* Have an urge to collaborate and communicate asynchronously.
* Have an urge to document all the things so you don't need to learn the same thing twice.
* Have a proactive, go-for-it attitude. When you see something broken, you can't help but fix it.
* Have an urge for delivering quickly and iterating fast.
* Share our [values](/handbook/values), and work in accordance with those
  values.
* Have experience with Docker, Nginx, Go, Kubernetes

## Projects you could work on:

* Coding infrastructure automation with [Chef](https://gitlab.com/gitlab-cookbooks/) and Terraform
* Improving our [Prometheus Monitoring](https://gitlab.com/gitlab-cookbooks/gitlab-prometheus) or building new [Metrics](https://gitlab.com/gitlab-org/gitlab-monitor)
* Helping [release managers](/handbook/engineering/#release-managers) deploy and troubleshoot new versions of GitLab-EE.
* Migrate GitLab.com from it’s current home on Azure Cloud to Google Cloud Platform.
* Migrate GitLab.com to Kubernetes.

## Areas of expertise for Leveling

* Chef
* Terraform
* CDN and load balancing the application
* Kubernetes and containerizing our system
* Product knowledge
* Monitoring and Metrics in Prometheus and integrations with Slack/PagerDuty
* Logging infrastructure
* Team organization and planning
* Backend storage management and scaling
* Disaster Recovery and High Availability strategy

## Leveling

### Junior Site Reliability Engineer

* Provides emergency response either by being on-call or by reacting to symptoms according to monitoring.
* Delivers production solutions that scale, identifying automation points, and proposing ideas on how to improve efficiency.
* Improves documentation all around, either in application documentation, or in runbooks, explaining the why, not stopping with the what.
* Improves the performance of the system by either making better use of resources, distributing load or reducing the latency.
* Shares the learnings publicly, either by creating issues that provide context for anyone to understand it or by writing blog posts.
* Updates GitLab default values so there is no need for configuration by customers.
* Improves monitoring and alerting fighting alert spam.
* General knowledge of the 2 of the areas of expertise

### Site Reliability Engineer

* Proposes ideas and solutions within the infrastructure team to reduce the workload by automation.
* Plan, design and execute solutions within infrastructure team to reach specific goals agreed within the team.
* Plan and execute configuration change operations both at the application and the infrastructure level.
* Actively looks for opportunities to improve the availability and performance of the system by applying the learnings from monitoring and observation
* 1 area of deep knowledge in the areas of expertise and general knowledge of on how to work with 4 more areas of expertise.

### Senior Site Reliability Engineer

Are experienced production engineers who meet the following criteria

* Lead Production SREs and Junior Production SREs by setting the example.
* Identifies changes for the product architecture from the reliability, performance and availability perspective with a data driven approach.
* Know a domain really well and radiate that knowledge
* Proactively work on the efficiency and capacity planning to set clear requirements and reduce the system resources usage to make GitLab cheaper to run for all our customers.
* Perform and run blameless root cause analyses on incidents and outages aggressively looking for answers that will prevent the incident from ever happening again.
* Show ownership of a major part of the infrastructure.
* Identify parts of the system that do not scale, provides immediate palliative measures and drives long term resolution of these incidents.
* Identify the SLI (Service Level Indicators) that will align the team to meet the availability and latency objectives.
* Deep knowledge in 2 areas of expertise and general knowledge of all areas of expertise.  Capable of mentoring Junior in all areas and other SRE in their area of deep knowledge.

### Staff Site Reliability Engineer

Senior Production SREs who meet the following criteria:

1. Technical Skills
  * Identifies significant projects that result in substantial cost savings or revenue
  * Able to create innovative solutions that push GitLab's technical abilities ahead of the curve
2. Leadership
	* Proposes and drives architectural changes that affects the whole company solve scaling and performance problems
	* Set the necessary goals and SLO (Service Level Objectives) that will guide the infrastructure team to build a better product
3. Communication
  * Writes in-depth documentation that shares knowledge and radiates GitLab technical strengths
4. Production, Scalability & Automation
	* Strives for automation either by coding it or by leading and influencing developers to build systems that are easy to run in production.
	* Measure the risk of introduced features to plan ahead and improve the infrastructure.
5. Deep knowledge of GitLab and 2 other areas of expertise.  Knowledge of each area of expertise enough to mentor and guide other team members in those areas.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates may receive a short questionnaire from our Global Recruiters
* Qualified candidates will be invited to schedule a 30min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters  

* Next, candidates will move to the first round of interviews
  * 60 Minute Technical Interview with a member for the SRE team. 
  * 45 Minute Peer Interview with 2 additional members of the SRE team
  * 60 Minute Interview with the Hiring Manager.  
* Next, candidates will move to the second round of interviews
  * 60 Minute Interview with the Director of Infrastructure Engineering
  * 60 Minute Interview with the VP of Engineering
* Successful candidates will subsequently be made an offer.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
