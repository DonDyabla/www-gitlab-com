---
layout: job_family_page
title: "People Business Partner"
---

The People Business Partner (PBP) position is responsible for aligning business objectives with employees and management in designated business units. In many companies, the role is referred to as HRBP. The position serves as a consultant to management on human resource-related issues. The successful HRBP acts as an employee champion and change agent. The position formulates partnerships across the People Ops function to deliver value-added service to management and team members that reflects the business objectives of the organization. The position will include international human resource responsibilities. The HRBP maintains an effective level of business literacy about the business unit's financial position, its midrange plans, its culture, and its competition. This role is intended to be solution-focused, and partner with and advise the business, without being “the HR police.”

### Director, HRBP

## Responsibilities

* Forms effective relationships with the client groups and consults with executive level management, providing People guidance when appropriate.
* Mentorship and coaching for People Ops Team Members.
* Ability to identify great talent, internally and externally, who will raise the bar on the team.
* Analyzes trends and metrics in partnership with the People Ops group to develop solutions, programs, and opportunities for learning
* Manages and resolves complex employee relations issues. Conducts effective, thorough and objective investigations.
* Maintains in-depth knowledge of legal requirements related to day-to-day management of team members, reducing legal risks and ensuring regulatory compliance. Partners with the legal department as needed/required.
* Works closely with management and employees to improve work relationships, build morale, and increase productivity and retention.
* Partners with team members globally to ensure a vibrant and effective workplace.
* Provides guidance and input on business unit restructures, workforce planning and succession planning to support the business strategy.
* Identifies training needs for business units and individual executive coaching needs. Participates in evaluation and monitoring of training programs to ensure success. Follows up to ensure training objectives are met.

## Requirements

* 10 years of experience as an HR Business, with at least 2 years of experience supporting Vice President level Team Members. 
* Strong attention to detail and ability to work well with fluid information.
* Comfortable using technology.
* Effective and concise verbal and written communication skills with the ability to collaborate with cross-functional team members. At times, courage is required to respectfully speak up, “push back”, challenge “the way things have always been done” and even disagree with leaders and executives.
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment
* Resourceful and takes initiative to seek internal and external resources when problem-solving
* Have implemented various learning and development programs that are aligned to the business needs
* Experience working with Global Teams
* Very strong EQ, with fine-tuned instincts and problem-solving skills.
* Strong drive and initiative with the ability to self-manage.

### Senior HRBP

## Responsibilities

* Provide strategic HR support to client groups in areas of performance management, management training and coaching, organizational development, and employee relations.
* Provides HR policy guidance and interpretation.
* Work closely with HR colleagues in implementing organization wide initiatives.
* Analyze trends and metrics in partnership with HR group to develop solutions, programs and policies.
* Raise concerns or trends to the CCO, legal, and/or HRBP directors to address timely and effectively.
* Being an enthusiastic team player with a strong drive to create a positive work environment.
* The ability to be comfortable with high volume workload and not be afraid to "roll up your sleeves".
* Experience working with Global teams. 


## Requirements

* 5 years of experience as an HR Business Partner.
* Strong attention to detail and ability to work well with fluid information
* Comfortable using technology.
* Effective and concise verbal and written communication skills with the ability to collaborate with cross-functional team members
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment
* Resourceful and takes initiative to seek internal and external resources when problem-solving
* Very strong EQ, with fine-tuned instincts and problem-solving skills.

### Intermediate HRBP

## Responsibilities

* Forms effective relationships with the client groups and consults with line management, providing People guidance when appropriate.
* Analyzes trends and metrics in partnership with the People Ops group to develop solutions, programs, and opportunities for learning
* Manages and resolves complex employee relations issues. Conducts effective, thorough and objective investigations.
* Maintains in-depth knowledge of legal requirements related to day-to-day management of employees, reducing legal risks and ensuring regulatory compliance. Partners with the legal department as needed/required.
* Works closely with management and employees to improve work relationships, build morale, and increase productivity and retention.
* Partners with colleagues outside the US to ensure a vibrant and effective workplace.
* Provides guidance and input on business unit restructures, workforce planning and succession planning.
* Identifies training needs for business units and individual executive coaching needs.
* Participates in evaluation and monitoring of training programs to ensure success. Follows up to ensure training objectives are met.

## Requirements

* 2 years of HR in recruiting, employee relations, and/or organizational development.
* Demonstrate discretion and sound judgment while working with sensitive and confidential materials.
* Comfortable using technology.
* Effective verbal and written communications.
* Passion for results.
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment.
* Resourceful and takes initiative to seek internal and external resources when problem-solving.
* Ability to solve moderate to complex problems
* Experience working with Global Talent in areas like Europe, India, and China
* Very strong EQ, with fine tuned instincts and problem solving skills.
