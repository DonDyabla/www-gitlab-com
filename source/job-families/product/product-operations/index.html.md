---
layout: job_family_page
title: "Product Operations"
---

## Role

At GitLab we take a unique approach to Product Management - one centered around
openness in our interaction with stakeholders, speed in the pace at which we iterate, 
simplicity in the single-application we build, and ambitiousness in what we aim to achieve.

Within the product group that often means we are consistently developing new process,
communication avenues, and engagement mechanisms to achieve these goals. Product Operations
team members recognize the unique way we work - and build tools to optimize the processes we develop.

We are looking for talented product operations professionals to join the Product team at GitLab. There
are millions of ways to re-invent the product management process for our values. Be the one
to make it a reality.

We encourage people to apply even if they don't have an established background
in this role yet, but they feel this page describes what they have a strong attitude for.
Developers with a strong interest in the product management process would be great fits for
this role.

We recommend looking at our [about page](/about) and at the [product handbook](https://about.gitlab.com/handbook/product/)
to get started.

## Responsibilities
- **Support the product organization**
  - Provide general support for the growth and [ambitious](/handbook/values/#results) goals of the product organization
  - Steward the overall [product development workflow](/handbook/product/#product-workflow) ensuring it runs efficiently
  - Facilitate the product wide initiatives required to maintain a [single-application](https://about.gitlab.com/direction/#vision)
- **Organize product lifecycle processes**
  - Assist in the creation of [devops-tools comparision](/devops-tools/) content
  - Recommend and implement optimizations to the [product development timeline](/handbook/product/#important-dates-pms-should-keep-in-mind)
- **Automate product internal and external communication**
  - Automate communication of the [Direction](/handbook/product/#stage-vision) and [Vision](/handbook/product/#category-vision) pages
  - Ensure smooth communication during the monthly [kick-off](/handbook/product/#kickoff-meeting)
  - Participate in the [creation of external content](/handbook/product/#writing-about-features) including blogs, webinar, and demos
  - Create visually compelling and interactive [roadmap](/handbook/product/#3-month-roadmap) views
  - Implement standard processes for [internal evangalism](/handbook/product/#internal-and-external-evangelization)
- **Facilitate improved product input from stakeholders**
  - Optimize the [product workflow](/handbook/product/#product-workflow) to encourage just-in-time customer and stakeholder input
  - Improve our [product discovery process](/handbook/product/#product-discovery-issues) to be optimized for async teams
  - Design and build systems to increase stakeholder [feedback](/handbook/product/#how-do-i-share-feedback) in GitLab issues
  - Develop intelligent tools for collecting and aggregating larger volumes of customer feedback

## Requirements

- Strong product instincts with an ability to understand customer pain points and how to translate those into a products
- A passion for building great products
- Strong understanding of Git and Git workflows
- Software developer with experience building web applications
- Strong experience with Ruby
- Knowledge of the developer tool space
- Highly independent and pragmatic
- Excellent proficiency in English
- You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
- You share our [values](/handbook/values), and work in accordance with those values
- Bonus points: experience with GitLab
- Bonus points: experience with Kubernetes and Cloud Native application development
- Bonus points: experience in working with open source projects

## Relevant links

- [Product Handbook](/handbook/product)