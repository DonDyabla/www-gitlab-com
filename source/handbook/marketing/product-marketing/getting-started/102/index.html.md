---
layout: markdown_page
title: "102 - Working at local speed"
---

**Objective:**  Faster website editing: the basics.
Want to be able to quickly make updates to the handbook or about.gitlab.com and not have to wait for every commit to go through a pipeline?   This workshop will show you how to setup git and docker on your local machine and get your work done 200% faster.

## Overview

**Comparison:** In-line edit vs WebIDE vs working locally
In-line editor, for super-quick minor edits


| **Approach** |  **Pros**  | **Cons** |
**WebIDE**  |  - WebIDE is simple to access <br> - WebIDE is awesome <br>- No environment setup <br> - No git commands necessary <br> - Easy to make quick edits <br> - One integrated flow (edit -> MR -> pipeline) | - No advanced git capability <br> - Full pipeline run to see effect of any changes <br> - Limited IDE functionality (compared to tools like Atom) |
| **Working locally** | - Work off-line <br> - Use whatever editor you want <br> - Most minimal feedback loop - see effect of changes immediately (just reload your browser)  |  - Need to remember to push back changes <br> - Need to know git commands <br> - Need to get setup (and maintain) local environment <br> - Need to understand working in a terminal |

### So why do locally?
- faster (tightest feedback loop)

### What does it take to work locally?

Not much:  Simply 3 things
1. a local copy of about.gitlab.com (git does this),
2. an editor or development tool (IDE) - I use Atom,
3. a way to build and run about.gitlab.com (docker)

#### A little terminal time

1. What’s a Terminal:  Simply a way to enter commands into your PC.  To work with Git and Docker, you need to use the Terminal.

1. a few key commands to navigate the terminal
   1. ls, ls -la, cd, cd .., pwd,  
   1. Folders and directories
   1. resources

#### Set up
The [read.me file from the www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com?nav_source=navbar) is a key reference for getting set up to work remotely.   Specifically, this section of the [read.me](https://gitlab.com/gitlab-com/www-gitlab-com?nav_source=navbar#use-docker-to-render-the-website).


1. Git - gets and manages your copy of about.gitlab.com
1. It’s time to Git Git
   1. What is Git
   From: https://git-scm.com/

   " Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.

   Git is easy to learn and has a tiny footprint with lightning fast performance. It outclasses SCM tools like Subversion, CVS, Perforce, and ClearCase with features like cheap local branching, convenient staging areas, and multiple workflows."

   Git is powerful and Git might be intimidating.  **(don't panic)** because GitLab makes it easy to use Git.

   Just in case - Bookmark or print the [Git CheatSheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)

[GitLab Basics](https://docs.gitlab.com/ee/gitlab-basics/README.html)
   1. [Check if Git has already been installed](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#check-if-git-has-already-been-installed)
   1. [Install and Configure Git](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#add-your-git-username-and-set-your-email)

   1. [Set up SHA key](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html)

   1. [Clone about.gitlab.com](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#add-your-git-username-and-set-your-email)
   1. a few git commands
      1. git pull, git branch, git checkout branch <branch name>


1. Docker: A docker container we call **“Middleman”** builds and runs about.gitlab.com
   1. What’s docker?
   "Docker is a tool designed to make it easier to create, deploy, and run applications by using containers. Containers allow a developer to package up an application with all of the parts it needs, such as libraries and other dependencies, and ship it all out as one package." (from https://opensource.com/resources/what-docker)
   1. Get Docker:   [Download the Mac version of Docker](https://www.docker.com/get-started)
   1. You will be asked to create a docker account, then download and install docker
   1. Configure Docker to build and run a local copy of about.gitlab.com   IN ONE COMMAND!
   See step 2 for the exact command [read.me file from the www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com?nav_source=navbar#use-docker-to-render-the-website)
   1. who’s the middleman? - Middleman is the name of the container that you just created.  
   1. a few docker commands
      1. docker start middleman, docker stop middleman
   1. docker resources
1. Get an IDE (such as Atom)  [Download and install Atom](https://atom.io/)

### Working Remotely.  (my workflow)

|  Step   | What it looks like |
|  1. In gitlab.com Create a new branch of about.gitlab.com   call it “name_of_branch” | ![new branch](/images/workshop/1_new_branch.png)   ![new branch](/images/workshop/2_create_branch.png) |
| 1. In gitlab.com - create a new MR for the branch I just created  | ![Create MR](/images/workshop/3_create_mr.png) ![MR Title](/images/workshop/4_mr_title.png) ![MR Details](/images/workshop/5_mr_details.png) |
| 1. In the MR - click on Checkout Branch and copy the commands | ![Checkout Branch](/images/workshop/6_checkout_branch.png) ![Copy Git Commands](/images/workshop/7_copy_git_commands.png)|
| 1. Local:   Open terminal | |
|   1. Local:   navigate to the Git directory where about.gitlab.com is stored.   Command: cd ww*   (use the wildecard * so I don’t have to type out ‘www.about.gitlab.com’ | ![Change Director](/images/workshop/terminal_cd.png) |
|   1. Local: Start the website running locally:  in the terminal type: docker start middleman  <br>   - this tells docker to start the container named middleman | image  |
|   1. Local: in the terminal Paste the checkout commands and hit enter <br>   - this will refresh your local copy of the project (website) with all the changes <br>  - and will tell git to point to the branch that you’ve created. | ![Paste terminal](/images/workshop/8_paste_in_terminal.png) ![Checkout Command (hit enter)](/images/workshop/9_checkout_command.png)  |
| 1. Local: Open Atom - the IDE to make edits. <br>   1. Local: in Atom - make a change to a file <br>    1. Local: in Atom - save the change |  images |
| 1. Local: in Browser- in the URL window - type: http://localhost:4567/ <br> - This will show you what the Middleman container sees in your local files.  You will instantly (almost) see the changes after you’ve saved your changes. |  images |
| 1. Local: in Atom - make and save more changes.   |  |
| 1. Local: in Browser - review changes  |  |
| 1. Local: when you’re done.  In Atom - On the ‘git tab’.  
 <br>   1. Stage your change <br>   1. Type a commit message that summarizes your change<br>   1. Click Commit to commit your changes to your local branch <br>   - keep working. (making changes, reviewing, committing)... But all of your work is only on your local workstation.  |   |
|  1. When you’re ready to push your changes back to the server.   Click on Push (command in lower right corner of Atom window) <br> - When you Push, your commits to your branch will be sent to the the Git server.  GitLab will see these changes and kick off a pipeline to build, test and deploy your changes to a review app.  |   |
| 1. In gitlab.com: When the pipeline finishes, review your changes and then have someone Approve your Merge Request.  |  |

### Summary:
Git
Docker
Terminal
IDE
