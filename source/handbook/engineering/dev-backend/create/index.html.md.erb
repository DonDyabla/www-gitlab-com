---
layout: markdown_page
title: "Create Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Create Team
{: #create}

The Create team is responsible for all backend aspects of the [Create stage of the DevOps lifecycle][stage],
which is all about creating code, and includes all functionality around source code management,
code review (merge requests), the Web IDE, wikis, and snippets.

For more details about the vision for this area of the product, please
see the [Create stage][stage] page and the [2019 product vision blog post][blog post].

[stage]: /handbook/product/categories/#create
[blog post]: /2018/09/21/create-vision/

### Team members

<%= direct_team(manager_role: 'Engineering Manager, Create') %>

### Stable counterparts

<%= stable_counterparts(role_regexp: /, Create/, direct_manager_role: 'Engineering Manager, Create') %>

## Work

In general, we use the standard GitLab [engineering workflow]. To get in touch
with the Create team, it's best to [create an issue] in the relevant project
(typically [GitLab CE]) and add the ~Create label, along with any other
appropriate labels. Then, feel free to ping the relevant Product Manager and/or
Engineering Manager as listed above.

For more urgent items, feel free to use [#g_create] on Slack.

[engineering workflow]: /handbook/engineering/workflow/
[create an issue]: /handbook/communication/#everything-starts-with-an-issue
[GitLab CE]: https://gitlab.com/gitlab-org/gitlab-ce

### What to work on

The primary source for things to work on is the [Create issue board] for the 
current release (don't forget to filter by milestone!), which lists all of the
issues scheduled for this release, in priority order: the deliverables. 
The backend and frontend lists are compiled by the Product Manager following 
the [product prioritization process], with input from the team, engineering
managers, and other stakeholders.

[By design][how-we-plan], we schedule more deliverables for each release than 
we can reasonably expect the team to deliver in that period. The expectation is
that the top 70% of deliverables on each list on the board will be done by the
[feature freeze] and will make it into the release they were scheduled for, 
while the next 30% will be moved to the next release, now with increased priority.

To ensure that the amount of scheduled work is reasonable considering these 
criteria, engineering managers estimate the weights of all issues the Product 
Manager puts forth for scheduling and limit the final scheduled set based on 
the issues' combined weight, historical team results, and the amount of time 
each engineer will be available to work on deliverables during this period.

#### What to work on first

The top 70% of deliverables are assigned to engineers on or ahead of the 8th of 
the month, and it is their responsibility to make a best effort to get them 
done by the feature freeze, and to inform their engineering manager if anything 
is standing in the way of their success.

You can find the issues assigned to you on the [Create backend assignment issue
board], which shows the same deliverables, now categorized by assignee, with the
left-most list listing issues not currently assigned to any backend engineer. 
On each list, the issues are still ordered by priority.

Many things can happen during a month that can result in an assigned deliverable
not actually making it into the intended release, and while this is usually not
a problem as long as a best effort was made, it should never come as a surprise 
to the engineering manager.
The sooner this potential outcome is anticipated and communicated, the more time
there is to see if anything can be done to prevent it, like reducing the scope 
of the deliverable or finding a different engineer who may have more time to 
finish a deliverable that hasn't been started yet. 
If this outcome cannot be averted and the deliverable ends up missing the 
release, it will simply be moved to the next release to be finished up, and the
engineer and engineering manager will have a chance to 
[retrospect](#retrospectives) and learn from what happened.

Generally, your assigned deliverables are expected to take up about 75% of the
time you spend working in a month. The other 25% is set aside for other 
responsibilities (code review, community merge request coaching, [helping 
people out in Slack, participating in discussions in issues][collaboration], 
etc), as well as urgent issues that come up during the month and need someone
working on them immediately (regressions, security issues, customer issues, etc).

#### What to work on next

If you have time to spare after finishing your top priority deliverables and 
other activities, you can spend the remaining time working on the lower priority
deliverables you can find in the left-most column of the [Create backend 
assignment issue board] (again, don't forget to filter by milestone!). 

Deliverables in the lower 30% of priority are usually not directly assigned to 
people, except in cases where there is clearly a most appropriate person to 
work on them, like in the case of technical debt or bugs related to work they
did recently. 
As mentioned above, these deliverables are not expected to be done by the 
feature freeze and to be moved to the next release, so any progress made on 
them ahead of time is a bonus.

Instead of picking up unassigned deliverables, you may also choose to spend any
spare time working on anything else that you believe will have a significant 
positive impact on the product or the company in general. 
As the [general guidelines] state, "we recognize that inspiration is 
perishable, so if you’re enthusiastic about something that generates great 
results in relatively little time feel free to work on that." 

We expect people to be [managers of one][efficiency] and prefer [responsibility
over rigidity][efficiency], so there's no need to ask for permission if you 
decide to work on something that's not on the issue board, but please keep your
other responsibilities in mind, and make sure that there is an issue, you are
assigned to it, and consider sharing it in [#g_create].

[Create issue board]: https://gitlab.com/groups/gitlab-org/-/boards/443693?label_name[]=Create&label_name[]=Deliverable
[Create backend assignment issue board]: https://gitlab.com/groups/gitlab-org/-/boards/936890
[product prioritization process]: /handbook/product/#prioritization
[how-we-plan]: /direction/#how-we-plan-releases
[feature freeze on the 7th]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#feature-freeze-on-the-7th-for-the-release-on-the-22nd
[collaboration]: /handbook/values/#collaboration
[general guidelines]: /handbook/general-guidelines/
[efficiency]: /handbook/values/#efficiency

### Retrospectives

The Create team conducts [monthly retrospectives in GitLab issues][retros]. These include
the backend team, plus any people from frontend, UX, and PM who have worked with
that team during the release being retrospected.

These are confidential during the initial discussion, then made public in time
for each month's [GitLab retrospective]. For more information, see [team
retrospectives].

[#g_create]: https://gitlab.slack.com/archives/g_create
[retros]: https://gitlab.com/gl-retrospectives/create-team/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=retrospective
[GitLab retrospective]: /handbook/engineering/workflow/#retrospective
[team retrospectives]: /handbook/engineering/management/team-retrospectives/
