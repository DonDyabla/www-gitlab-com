---
layout: markdown_page
title: "Recruiting Alignment"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Recruiter and Coordinator Alignment by Hiring Manager

| Sales                    | Recruiter       | Coordinator     |
|--------------------------|-----------------|-----------------|
| Michael McBride          | Kelly Murdock   | April Hoffbauer |
| Francis Aquino           | Kelly Murdock   | April Hoffbauer |
| Paul Almeida             | Kelly Murdock   | April Hoffbauer |
| Kristen Lawrence         | Kelly Murdock   | April Hoffbauer |
| Richard Pidgeon          | Nadia Vatalidis | Kike Adio       |

| Marketing                                      | Recruiter       | Coordinator     |
|------------------------------------------------|-----------------|-----------------|
| Erica Lindberg                                 | Jacie Zoerb     | Chantal Rollison |
| Ashish Kuthiala                                | Jacie Zoerb     | Chantal Rollison |
| LJ Banks                                       | Jacie Zoerb     | Chantal Rollison |
| Alex Turner                                    | Jacie Zoerb     | Chantal Rollison |
| Director of Field Marketing (to be hired, Leslie Blanchard as interim) | Jacie Zoerb     | Chantal Rollison |
| Director of Marketing Operations (to be hired, LJ Banks as interim) | Jacie Zoerb     | Chantal Rollison |
| David Planella                                 | Jacie Zoerb     | Chantal Rollison |
| Melissa Smolensky                              | Jacie Zoerb     | Chantal Rollison |
| Elsje Smart                                    | Jacie Zoerb     | Chantal Rollison |

| Engineering           | Recruiter                                               | Coordinator |
|-----------------------|---------------------------------------------------------|-------------|
| Frontend              | Eva Petreska                                            | Emily Mowry |
| Quality               | Eva Petreska                                            | Emily Mowry |
| UX                    | Eva Petreska                                            | Emily Mowry |
| Support               | Steve Pestorich                                         | Emily Mowry |
| Infrastructure        | Matt Allen                                              | Emily Mowry |
| Security              | Steve Pestorich                                         | Emily Mowry |
| Backend - Dev         | Liam McNally                                            | Kike Adio   |
| Backend - Secure      | Liam McNally                                            | Kike Adio   |
| Backend - Ops         | Trust Ogor                                              | Kike Adio   |
| Backend - Enablement  | Trust Ogor                                              | Kike Adio   |

| Product           | Recruiter                       | Coordinator |
|-------------------|---------------------------------|-------------|
| Mark Pundsack     | Matt Allen                      | Emily Mowry |

| Other         | Recruiter                       | Coordinator               |
|---------------|---------------------------------|---------------------------|
| Paul Machle   | Jacie Zoerb/Stephanie Garza     | Chantal Rollison/April Hoffbauer |
| Carol Teskey  | Jacie Zoerb/Kelly Murdock       | Chantal Rollison/April Hoffbauer |
| Brandon Jung  | Kelly Murdock                   | April Hoffbauer           |
| Meltano       | Steve Pestorich                 | Emily Mowry               |

## Sourcer Alignment by Division and Location

| Product               | Region            | Sourcer                | Estimated % |
|-----------------------|-------------------|------------------------|-------------|
| Sales & Marketing     | Americas/Anywhere | Stephanie Garza        | 100%        |
| Sales & Marketing     | EMEA & APAC       | Anastasia Pshegodskaya | 25%         |
| Engineering Backend   | Anywhere          | Zsuzsanna Kovacs       | 100%        |
| Engineering General   | Anywhere          | Anastasia Pshegodskaya | 50%         |
| Other/Director+ Roles | Anywhere          | Anastasia Pshegodskaya | 25%         |
